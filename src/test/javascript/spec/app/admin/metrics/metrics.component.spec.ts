import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { SkylarkTestModule } from '../../../test.module';
import { AceMetricsMonitoringComponent } from 'app/admin/metrics/metrics.component';
import { AceMetricsService } from 'app/admin/metrics/metrics.service';

describe('Component Tests', () => {
    describe('AceMetricsMonitoringComponent', () => {
        let comp: AceMetricsMonitoringComponent;
        let fixture: ComponentFixture<AceMetricsMonitoringComponent>;
        let service: AceMetricsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SkylarkTestModule],
                declarations: [AceMetricsMonitoringComponent]
            })
                .overrideTemplate(AceMetricsMonitoringComponent, '')
                .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AceMetricsMonitoringComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AceMetricsService);
        });

        describe('refresh', () => {
            it('should call refresh on init', () => {
                // GIVEN
                const response = {
                    timers: {
                        service: 'test',
                        unrelatedKey: 'test'
                    },
                    gauges: {
                        'jcache.statistics': {
                            value: 2
                        },
                        unrelatedKey: 'test'
                    }
                }
                spyOn(service, 'getMetrics').and.returnValue(of((response)));

                // WHEN
                comp.ngOnInit()

                // THEN
                expect(service.getMetrics).toHaveBeenCalled();
            });
        });
    });
});
