import { NgModule } from '@angular/core';

import {
    SkylarkSharedLibsModule,
    AceAlertComponent,
    AceAlertErrorComponent
} from './';

@NgModule({
    imports: [
        SkylarkSharedLibsModule
    ],
    declarations: [
        AceAlertComponent,
        AceAlertErrorComponent
    ],
    exports: [
        SkylarkSharedLibsModule,
        AceAlertComponent,
        AceAlertErrorComponent
    ]
})
export class SkylarkSharedCommonModule {}
