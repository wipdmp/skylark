import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import {
    SkylarkSharedLibsModule,
    SkylarkSharedCommonModule,
    AceLoginModalComponent,
    HasAnyAuthorityDirective
} from './';

@NgModule({
    imports: [
        SkylarkSharedLibsModule,
        SkylarkSharedCommonModule
    ],
    declarations: [
        AceLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        { provide: NgbDateAdapter, useClass: NgbDateMomentAdapter},
    ],
    entryComponents: [AceLoginModalComponent],
    exports: [
        SkylarkSharedCommonModule,
        AceLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class SkylarkSharedModule {
    static forRoot() {
        return {
            ngModule: SkylarkSharedModule
        };
    }
}
