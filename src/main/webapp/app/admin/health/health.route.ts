import { Route } from '@angular/router';

import { AceHealthCheckComponent } from './health.component';

export const healthRoute: Route = {
    path: 'ace-health',
    component: AceHealthCheckComponent,
    data: {
        pageTitle: 'Health Checks'
    }
};
