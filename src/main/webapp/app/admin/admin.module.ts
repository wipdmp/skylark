import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SkylarkSharedModule } from 'app/shared';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

import {
    adminState,
    AuditsComponent,
    UserMgmtComponent,
    UserMgmtDetailComponent,
    UserMgmtUpdateComponent,
    UserMgmtDeleteDialogComponent,
    LogsComponent,
    AceMetricsMonitoringComponent,
    AceHealthModalComponent,
    AceHealthCheckComponent,
    AceConfigurationComponent,
    AceDocsComponent,
} from './';

@NgModule({
    imports: [
        SkylarkSharedModule,
        RouterModule.forChild(adminState),
        /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    ],
    declarations: [
        AuditsComponent,
        UserMgmtComponent,
        UserMgmtDetailComponent,
        UserMgmtUpdateComponent,
        UserMgmtDeleteDialogComponent,
        LogsComponent,
        AceConfigurationComponent,
        AceHealthCheckComponent,
        AceHealthModalComponent,
        AceDocsComponent,
        AceMetricsMonitoringComponent
    ],
    entryComponents: [
        UserMgmtDeleteDialogComponent,
        AceHealthModalComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SkylarkAdminModule {
}
