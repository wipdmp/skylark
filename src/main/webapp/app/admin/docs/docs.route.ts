import { Route } from '@angular/router';

import { AceDocsComponent } from './docs.component';

export const docsRoute: Route = {
    path: 'docs',
    component: AceDocsComponent,
    data: {
        pageTitle: 'API'
    }
};
