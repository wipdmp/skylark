import { Route } from '@angular/router';

import { AceMetricsMonitoringComponent } from './metrics.component';

export const metricsRoute: Route = {
    path: 'ace-metrics',
    component: AceMetricsMonitoringComponent,
    data: {
        pageTitle: 'Application Metrics'
    }
};
