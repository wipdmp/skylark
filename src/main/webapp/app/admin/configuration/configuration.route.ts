import { Route } from '@angular/router';

import { AceConfigurationComponent } from './configuration.component';

export const configurationRoute: Route = {
    path: 'ace-configuration',
    component: AceConfigurationComponent,
    data: {
        pageTitle: 'Configuration'
    }
};
