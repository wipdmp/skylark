/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wipro.Skylark.web.rest.vm;
